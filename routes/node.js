const express = require('express');
const fetch = require('node-fetch');

const app = express();

app.get('/node', function(req, res) {

    var respuesta = '';

    fetch('http://usuarios:8080/usuario')
        .then(response => response.text())
        .then(data => {
            console.log('Repuesta ' + data);
            respuesta = data;
            res.json({nombre:'Gerardo Chavez', respuesta});
        })
    .catch(err => console.log(err))

});

module.exports = app;